<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Article;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $articles = Article::all();
        return view('home', compact('articles'));

       /* $articles = Article::find('email');
        return view('home', ['articles' => $articles]);*/

    }

    public function store(Request $request){
        $article = new Article();
        $article->user = Input::get('user');
        $article->category =Input::get('auction_category');
        $article->auction_name = Input::get('item_name');
        $article->description = Input::get('description');
        $article->price =Input::get('item_price');
       if(Input::hasFile('image')){
           $file = Input::file('image');
           $file->move(public_path(). '/images', $file->getClientOriginalName());
           $article->image = $file->getClientOriginalName();
       }
        $article->save();
        return " Added Successfully";
    }

    public function showAll(){
        $posts = Article::all();
        return view('welcome',  compact('posts') );
    }

    public function post()
    {
        return view('post');
    }
}
