@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                   <h3>Post an item</h3>
                        <br>
                    <form method="POST" action="{{url('/insert')}}" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="auction_category">Example select</label>
                            <select class="form-control" id="auction_category" name="auction_category">
                                <option value="0">Select a Category</option>
                                <option value="Clothing">Clothing</option>
                                <option value="Electronics">Electronic</option>
                                <option value="Home">Home & Garden</option>
                            </select>
                        </div>


                            <input type="hidden" class="form-control" id="user" name="user" value="{{ Auth::user()->email }}">


                            <div class="form-group">
                                <label for="item_name">Auction Title</label>
                                <input type="text" class="form-control" id="item_name" name="item_name" placeholder="Used Shirt">
                            </div>

                            <div class="form-group">
                                <label for="item_name">Description</label>
                                <input type="text" class="form-control" id="description" name="description" placeholder="Used Shirt">
                            </div>

                        <div class="form-group">
                            <label for="item_name">Price</label>
                            <input type="number" class="form-control" id="item_price" name="item_price" placeholder="$20">
                        </div>

                        <div class="form-group">
                            <label for="image">Product Image</label>
                            <input type="file" class="form-control-file" id="image" name="image">
                        </div>

                        <button type="submit" class="btn btn-primary">Post</button>

                    </form>

                </div>
            </div>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Auction Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Price</th>
                </tr>
                </thead>
                <tbody>

                    @if(count($articles) > 0)
                        @foreach($articles->all() as $article))

                <tr>
                    <th scope="row">{{ $article->id }}</th>
                    <td>{{ $article->auction_name }}</td>
                    <td>{{ $article->description }}</td>
                    <td>{{ $article->price }}</td>
                </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
