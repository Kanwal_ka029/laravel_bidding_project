@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">ADMIN Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in as <strong>ADMIN</strong>!

                        <div class="container">

                            @foreach($user as $users)

                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$users->name}}</h5>
                                        <p class="card-text">{{$users->email}}</p>

                                    </div>
                                </div>


                            @endforeach
                        </div
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
